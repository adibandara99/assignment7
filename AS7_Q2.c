#include <stdio.h>
#include <string.h>
int main()
{
    char word[100],character;
    int count=0;
    printf("Enter the string: ");
    fgets(word, sizeof(word), stdin);
    printf("Enter the character that you want to find the frequency: ");
    scanf("%c",&character);
    for (int i=0;word[i]!='\0';++i)
    {
        if (character==word[i])
        {
            ++count;
        }
    }
    printf("Frequency of the %c is %d",character,count);
    return 0;
}
