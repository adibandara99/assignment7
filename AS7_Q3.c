#include <stdio.h>
/*
getM(M2,r,c);
Addition(Result,M1,M2,r,c);
Multiply(Result,M1,M2,r,c1,c2);
Display(Result,r,c);
*/
void Addition(int Result[][10],int M1[][10],int M2[][10],int r,int c)
{
    printf("Addition of Matrix\n");
    for (int i=0;i<r;i++)
    {
        for (int j=0;j<c;j++)
        {
            Result[i][j]=M1[i][j]+M2[i][j];
            printf("%d ",Result[i][j]);
        }
        printf("\n");
    }
}
void Display(int Result[][10],int r,int c)
{
    printf("Multiplication of Matrix\n");
    for (int i = 0; i < r; ++i) {
      for (int j = 0; j < c; ++j) {
         printf("%d  ", Result[i][j]);
         if (j == c - 1)
            printf("\n");
      }
   }
}

void Multiply(int Result[][10],int M1[][10],int M2[][10],int r,int c1,int c2)
{
    int i,j,k;
    for (i=0;i<r;i++)
    {
        for (j=0;j<c2;j++)
        {
            Result[i][j]=0;
        }
    }
    for (i=0;i<r;i++)
    {
        for (j=0;j<c1;j++)
        {
            for (k=0;k<c2;k++)
            {
                Result[i][j]+=M1[i][k]*M2[k][j];
            }
        }
    }
}

void getM(int M[][10],int r,int c)
{
    for (int i=0;i<r;i++)
    {
        for (int j=0;j<c;j++)
        {
            scanf("%d",&M[i][j]);
        }
    }
}

int main()
{
    int M1[10][10],M2[10][10],Result[10][10];
    printf("Enter elements for the first matrix \n");
    getM(M1,2,2);
    printf("Enter elements for the second matrix \n");
    getM(M2,2,2);
    Addition(Result,M1,M2,2,2);
    Multiply(Result,M1,M2,2,2,2);
    Display(Result,2,2);

    return 0;
}

